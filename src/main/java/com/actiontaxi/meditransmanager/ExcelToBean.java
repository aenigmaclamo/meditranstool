/*
 * Copyright 2016 Kevin Raoofi.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.actiontaxi.meditransmanager;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConversionException;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;
import org.apache.commons.beanutils.converters.DateConverter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;

/**
 * Provides a simple API to take a Sheet from Apache POI and converts it into a
 * list of beans.
 *
 * @author Kevin Raoofi
 * @param <T> type for the Bean to convert excel values to
 */
public class ExcelToBean<T> {

    /**
     * Supplies the instance for BeanUtils to do conversion
     */
    private static final BeanUtilsBean bub;
    /**
     * Supplied to BeanUtilsBean to do Excel specific conversions
     */
    private static final ConvertUtilsBean cub;
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(
            ExcelToBean.class.getName());

    static {
        // Register the ConverUtilsBean into the BeanUtilsBean
        cub = new ConvertUtilsBean();
        cub.register(new ExcelDateConverter(), Date.class);
        bub = new BeanUtilsBean(cub);
    }

    /**
     * Gets the headings by looking through an iterator representing the first
     * row. It translates the headings so that the first case is lower case so
     * that it works with BeanUtils
     * <p>
     * Additionally, this method assumes that all cells are non-empty, string,
     * and not null.
     *
     * @param headingsIterator the first row
     * @return a map of all the positions of the headings to the heading
     */
    private static Map<Integer, String> getHeadings(
            final Iterator<Cell> headingsIterator) {
        final Map<Integer, String> headings = new TreeMap<>();

        int i = 0;

        while (headingsIterator.hasNext()) {
            final Cell heading = headingsIterator.next();
            String headingStr = heading.getStringCellValue();

            headingStr = headingStr.substring(0, 1).toLowerCase() + headingStr.
                    substring(1);
            headings.put(i, headingStr);
            i++;
        }

        return headings;
    }

    /**
     * Creates a Map that represents the object as Key -> Value pairs
     *
     * @param headings the names of the keys comes from here based on the order
     *                 in which it is shown in the map
     * @param row      the actual row which becomes a representation of the
     *                 object
     * @return map representing object
     */
    private static Map<String, Object> populateObjectMap(
            Map<Integer, String> headings, Map<Integer, Cell> row) {
        final HashMap<String, Object> hm = new HashMap<>();

        for (Map.Entry<Integer, Cell> entry : row.entrySet()) {
            final String heading = headings.get(entry.getKey());
            final Cell cell = entry.getValue();

            switch (cell.getCellTypeEnum()) {
                case NUMERIC:
                    hm.put(heading, cell.getNumericCellValue());
                    break;
                case STRING:
                    hm.put(heading, cell.getStringCellValue());
                    break;
                case BOOLEAN:
                    hm.put(heading, cell.getBooleanCellValue());
                    break;
                case BLANK:
                    hm.put(heading, null);
                    break;
                default:
                    LOG.log(Level.WARNING,
                            "Got unexpected cell type: {0}, contents: {1}",
                            new Object[]{cell.getCellTypeEnum(), cell});
            }
        }
        return hm;
    }

    /**
     * Utility method to turn a cell into the cell reference used in Excel with
     * the sheet name
     *
     * @param cell excel cell to reference
     * @return string representation of cell reference with sheet name
     */
    private static String cellToCellRefStr(Cell cell) {
        return cell.getSheet().getSheetName()
                + "!" + cell.getAddress().formatAsString();
    }

    /**
     * A constructor to generate the desired bean
     */
    private final Supplier<T> constructor;
    private final Row.MissingCellPolicy policy;

    /**
     * Creates an ExcelToBean using a supplier for a bean
     *
     * @param constructor supply of the bean object
     */
    public ExcelToBean(Supplier<T> constructor) {
        this.constructor = constructor;
        this.policy = Row.MissingCellPolicy.CREATE_NULL_AS_BLANK;
    }

    /**
     * Converts a sheet from Apache POI to a list of beans
     *
     * @param sheet
     * @return list of beans
     */
    public List<T> sheetToBean(final Sheet sheet) {
        // Effectively, this converts the sheet into an area and passed off the
        // hard work to areaToBean
        if (!sheet.iterator().hasNext()) {
            throw new IllegalStateException(
                    "First row in sheet is empty; cannot deduce row headings");
        }

        final Row firstRow = sheet.getRow(0);
        final Row lastRow = sheet.getRow(sheet.getLastRowNum());

        final Cell topLeft = firstRow.getCell(0);

        final Cell bottomRight = lastRow.getCell(Math.max(
                lastRow.getLastCellNum() - 1,
                firstRow.getLastCellNum() - 1
        ), this.policy);

        final AreaReference area = new AreaReference(
                new CellReference(cellToCellRefStr(topLeft)),
                new CellReference(cellToCellRefStr(bottomRight))
        );

        return areaToBean(area, sheet.getWorkbook());
    }

    /**
     * Creates "rows" from an area and turns them into a list of beans
     *
     * @param area the area must be created with cell references which have the
     *             sheet names or there will be a NPE
     *
     * @param wb   workbook that the area is in
     * @return list of beans
     */
    List<T> areaToBean(AreaReference area, Workbook wb) {
        // I'm not sure if this method is ready for prime time, yet
        final List<T> results = new ArrayList<>();
        final CellReference[] references = area.getAllReferencedCells();
        final Map<Integer, Map<Integer, Cell>> table = new TreeMap<>();

        for (CellReference ref : references) {
            final Sheet sheet = wb.getSheet(ref.getSheetName());
            final Row row = sheet.getRow(ref.getRow());
            final Cell cell = row.getCell(ref.getCol(), this.policy);

            final int tableRowNum = ref.getRow()
                    - area.getFirstCell().getRow();
            final int tableColumnNum = ref.getCol()
                    - area.getFirstCell().getCol();

            final Map<Integer, Cell> tableRow = table.getOrDefault(
                    tableRowNum, new TreeMap<>());
            tableRow.put(tableColumnNum, cell);
            table.put(tableRowNum, tableRow);
        }

        final Map<Integer, String> headings = getHeadings(table.remove(0)
                .values()
                .iterator());

        for (Map.Entry<Integer, Map<Integer, Cell>> entry : table.entrySet()) {
            final Map<String, Object> hm = populateObjectMap(
                    headings, entry.getValue());

            try {
                final T man = this.constructor.get();
                bub.populate(man, hm);

                results.add(man);

            } catch (IllegalAccessException | InvocationTargetException ex) {
                Logger.getLogger(ExcelToBean.class
                        .getName()).log(Level.SEVERE,
                                null, ex);
            }
        }

        return results;
    }

    /**
     * A converted which uses DateUtils from POI to convert Excel Date to
     * java.util.Date
     */
    private static class ExcelDateConverter implements Converter {

        @Override
        public <T> T convert(Class<T> type, Object value) {
            if (!type.equals(Date.class)) {
                throw new ConversionException("Can only convert to "
                        + Date.class.getSimpleName() + " classes!");
            }

            if (!value.getClass().equals(Double.class)) {
                return new DateConverter().convert(type, value);
            }

            double v = (Double) value;

            return (T) DateUtil.getJavaDate(v);
        }
    }
}
