/*
 * Copyright 2016 kesari.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.actiontaxi.meditransmanager;

import com.actiontaxi.meditransmanager.models.Manifest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author kesari
 */
public class MeditransConsoleDriver {

    static Workbook getUserInput(File manifestLocation) throws
            BackingStoreException, IOException, InvalidFormatException {
        final Preferences prefs = Preferences.userRoot().node(
                "com.actiontaxi.meditransmanager");

        String password = prefs.get("excelPassword", "");
        final Scanner sc = new Scanner(System.in);

        System.out.println("Loaded password: " + password);
        System.out.println("Change password? Y/N");

        final String input = sc.nextLine();

        switch (input) {
            case "clear":
                System.out.println("Clearing preferences...");
                prefs.clear();
            case "Y":
            case "y":
                System.out.println("Enter new password: ");
                prefs.put("excelPassword", sc.nextLine());
                password = prefs.get("excelPassword", "");
                System.out.println("Loaded password: " + password);
        }

        System.out.println("Continuing...");

        return WorkbookFactory.create(manifestLocation, password, true);
    }

    public static void main(String... args) throws IOException,
            InvalidFormatException, BackingStoreException {
        final File sampleManifest = new File(
                "C:/Users/kesari/Desktop/meditrans/Manifests/"
                + "Manifest-ActionTaxiInc-2016-11-22.xls");

        final Workbook wb = getUserInput(sampleManifest);
//
//        for (Name name : wb.getAllNames()) {
//            String formula = name.getRefersToFormula();
//            AreaReference area = new AreaReference(formula,
//                    wb.getSpreadsheetVersion());
//        }

        ExcelToBean<Manifest> excelToBean = new ExcelToBean(Manifest::new);

        List<Manifest> manifests = excelToBean.sheetToBean(wb.getSheetAt(0));
        System.out.println(manifests);
    }
}
