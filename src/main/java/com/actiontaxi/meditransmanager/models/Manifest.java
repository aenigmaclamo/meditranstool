/*
 * Copyright 2016 kesari.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.actiontaxi.meditransmanager.models;

import java.beans.Transient;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Kevin Raoofi
 */
public class Manifest {

    private static final DateTimeFormatter EXCEL_TIME = DateTimeFormatter
            .ofPattern("hh:mm[ a]");

    private static final LocalTime DEFAULT_TIME = LocalTime.of(12, 0);

    public String Provider;
    public String ServiceDesc;
    public Date TripDate;
    public String TripTimeAMPM;
    public String TripPUCode;
    public String ClientFullName;
    public String TripNo;
    public String TripOriginPh;
    public String TripOriginStreet;
    public String TripOriginCity;
    public String TripOriginState;
    public String TripOriginZIP;
    public String TripDestStreet;
    public String TripDestCity;
    public String TripDestState;
    public String TripDestZIP;
    public String TripComments;
    public String ProvCode;

    public String getProvider() {
        return Provider;
    }

    public void setProvider(String Provider) {
        this.Provider = Provider;
    }

    public String getServiceDesc() {
        return ServiceDesc;
    }

    public void setServiceDesc(String ServiceDesc) {
        this.ServiceDesc = ServiceDesc;
    }

    public Date getTripDate() {
        return TripDate;
    }

    public void setTripDate(Date TripDate) {
        this.TripDate = TripDate;
    }

    public String getTripTimeAMPM() {
        return TripTimeAMPM;
    }

    public void setTripTimeAMPM(String TripTimeAMPM) {
        this.TripTimeAMPM = TripTimeAMPM;
    }

    public String getTripPUCode() {
        return TripPUCode;
    }

    public void setTripPUCode(String TripPUCode) {
        this.TripPUCode = TripPUCode;
    }

    public String getClientFullName() {
        return ClientFullName;
    }

    public void setClientFullName(String ClientFullName) {
        this.ClientFullName = ClientFullName;
    }

    public String getTripNo() {
        return TripNo;
    }

    public void setTripNo(String TripNo) {
        this.TripNo = TripNo;
    }

    public String getTripOriginPh() {
        return TripOriginPh;
    }

    public void setTripOriginPh(String TripOriginPh) {
        this.TripOriginPh = TripOriginPh;
    }

    public String getTripOriginStreet() {
        return TripOriginStreet;
    }

    public void setTripOriginStreet(String TripOriginStreet) {
        this.TripOriginStreet = TripOriginStreet;
    }

    public String getTripOriginCity() {
        return TripOriginCity;
    }

    public void setTripOriginCity(String TripOriginCity) {
        this.TripOriginCity = TripOriginCity;
    }

    public String getTripOriginState() {
        return TripOriginState;
    }

    public void setTripOriginState(String TripOriginState) {
        this.TripOriginState = TripOriginState;
    }

    public String getTripOriginZIP() {
        return TripOriginZIP;
    }

    public void setTripOriginZIP(String TripOriginZIP) {
        this.TripOriginZIP = TripOriginZIP;
    }

    public String getTripDestStreet() {
        return TripDestStreet;
    }

    public void setTripDestStreet(String TripDestStreet) {
        this.TripDestStreet = TripDestStreet;
    }

    public String getTripDestCity() {
        return TripDestCity;
    }

    public void setTripDestCity(String TripDestCity) {
        this.TripDestCity = TripDestCity;
    }

    public String getTripDestState() {
        return TripDestState;
    }

    public void setTripDestState(String TripDestState) {
        this.TripDestState = TripDestState;
    }

    public String getTripDestZIP() {
        return TripDestZIP;
    }

    public void setTripDestZIP(String TripDestZIP) {
        this.TripDestZIP = TripDestZIP;
    }

    public String getTripComments() {
        return TripComments;
    }

    public void setTripComments(String TripComments) {
        this.TripComments = TripComments;
    }

    public String getProvCode() {
        return ProvCode;
    }

    public void setProvCode(String ProvCode) {
        this.ProvCode = ProvCode;
    }

    @Transient
    public Instant getJobInstant() {
        final LocalTime timeOfTrip;
        if (this.TripTimeAMPM == null) {
            timeOfTrip = DEFAULT_TIME;
        } else {
            timeOfTrip = LocalTime.parse(this.TripTimeAMPM, EXCEL_TIME);
        }

        final LocalDateTime ldt = LocalDateTime.of(this.TripDate
                .toInstant().atZone(ZoneId.systemDefault())
                .toLocalDate(), timeOfTrip);

        return ldt.atZone(ZoneId.systemDefault()).toInstant();

    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + Objects.hashCode(this.Provider);
        hash = 19 * hash + Objects.hashCode(this.ServiceDesc);
        hash = 19 * hash + Objects.hashCode(this.TripDate);
        hash = 19 * hash + Objects.hashCode(this.TripTimeAMPM);
        hash = 19 * hash + Objects.hashCode(this.TripPUCode);
        hash = 19 * hash + Objects.hashCode(this.ClientFullName);
        hash = 19 * hash + Objects.hashCode(this.TripNo);
        hash = 19 * hash + Objects.hashCode(this.TripOriginPh);
        hash = 19 * hash + Objects.hashCode(this.TripOriginStreet);
        hash = 19 * hash + Objects.hashCode(this.TripOriginCity);
        hash = 19 * hash + Objects.hashCode(this.TripOriginState);
        hash = 19 * hash + Objects.hashCode(this.TripOriginZIP);
        hash = 19 * hash + Objects.hashCode(this.TripDestStreet);
        hash = 19 * hash + Objects.hashCode(this.TripDestCity);
        hash = 19 * hash + Objects.hashCode(this.TripDestState);
        hash = 19 * hash + Objects.hashCode(this.TripDestZIP);
        hash = 19 * hash + Objects.hashCode(this.TripComments);
        hash = 19 * hash + Objects.hashCode(this.ProvCode);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Manifest other = (Manifest) obj;
        if (!Objects.equals(this.Provider, other.Provider)) {
            return false;
        }
        if (!Objects.equals(this.ServiceDesc, other.ServiceDesc)) {
            return false;
        }
        if (!Objects.equals(this.TripTimeAMPM, other.TripTimeAMPM)) {
            return false;
        }
        if (!Objects.equals(this.TripPUCode, other.TripPUCode)) {
            return false;
        }
        if (!Objects.equals(this.ClientFullName, other.ClientFullName)) {
            return false;
        }
        if (!Objects.equals(this.TripNo, other.TripNo)) {
            return false;
        }
        if (!Objects.equals(this.TripOriginPh, other.TripOriginPh)) {
            return false;
        }
        if (!Objects.equals(this.TripOriginStreet, other.TripOriginStreet)) {
            return false;
        }
        if (!Objects.equals(this.TripOriginCity, other.TripOriginCity)) {
            return false;
        }
        if (!Objects.equals(this.TripOriginState, other.TripOriginState)) {
            return false;
        }
        if (!Objects.equals(this.TripOriginZIP, other.TripOriginZIP)) {
            return false;
        }
        if (!Objects.equals(this.TripDestStreet, other.TripDestStreet)) {
            return false;
        }
        if (!Objects.equals(this.TripDestCity, other.TripDestCity)) {
            return false;
        }
        if (!Objects.equals(this.TripDestState, other.TripDestState)) {
            return false;
        }
        if (!Objects.equals(this.TripDestZIP, other.TripDestZIP)) {
            return false;
        }
        if (!Objects.equals(this.TripComments, other.TripComments)) {
            return false;
        }
        if (!Objects.equals(this.ProvCode, other.ProvCode)) {
            return false;
        }
        if (!Objects.equals(this.TripDate, other.TripDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Manifest{" + "Provider=" + Provider + ", ServiceDesc="
                + ServiceDesc + ", TripDate=" + TripDate + ", TripTimeAMPM="
                + TripTimeAMPM + ", TripPUCode=" + TripPUCode
                + ", ClientFullName=" + ClientFullName + ", TripNo=" + TripNo
                + ", TripOriginPh=" + TripOriginPh + ", TripOriginStreet="
                + TripOriginStreet + ", TripOriginCity=" + TripOriginCity
                + ", TripOriginState=" + TripOriginState + ", TripOriginZIP="
                + TripOriginZIP + ", TripDestStreet=" + TripDestStreet
                + ", TripDestCity=" + TripDestCity + ", TripDestState="
                + TripDestState + ", TripDestZIP=" + TripDestZIP
                + ", TripComments=" + TripComments + ", ProvCode=" + ProvCode
                + '}';
    }

}
